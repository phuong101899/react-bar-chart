import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const CANVAS = {
	HEIGHT: 500,
	WIDTH: 800
}

const COLUMN_WIDTH = 50;
const COLUMN_MARGIN = 10;

class App extends Component {

	rects = [];

	data = [
		{color: 'red', height: 100},
		{color: 'blue', height: 150},
		{color: 'green', height: 90},
		{color: 'orange', height: 200},
		{color: 'brown ', height: 300},
		{color: 'DeepPink', height: 300},
		{color: 'LightCoral', height: 250}
	];


	componentDidMount() {
		this.ctx = this.canvas.getContext('2d');
		
		this.data.forEach((item, index) => {
			let x = (COLUMN_WIDTH * index) + (COLUMN_MARGIN * index);
			let rect = this.buildColumn(this.ctx, {color: item.color, x: x, height: item.height});
			rect.isPointInside = function(x, y){
				return (x >= this.x && x <= this.x + this.width && y >= this.y && y <= this.y + this.height);
			}

			this.rects.push(rect);
		});

	}

	buildColumn = (ctx, {color, x, height, hoverColor}) => {

		let y = CANVAS.HEIGHT - height;

		ctx.beginPath();
		ctx.rect(x, y, COLUMN_WIDTH, height);
		ctx.fillStyle = color;

		ctx.shadowBlur = 1;
		ctx.shadowColor = "white";

		if(hoverColor) {
			ctx.strokeStyle = hoverColor;
		}
		
		ctx.fill();
		ctx.stroke();
		
		ctx.fillText(`${height}%`, x + 15, y - 5, COLUMN_WIDTH);

		return {
			x,
			y,
			width: COLUMN_WIDTH,
			height,
			color
		};
	}

	onMouseMove = (e) => {
		let offsetX = this.canvas.offsetLeft;
		let offsetY = this.canvas.offsetTop;
		
		let mouseX = parseInt(e.clientX - offsetX, 10);
		let mouseY = parseInt(e.clientY - offsetY, 10);

		this.rects.forEach(item => {
			if (item.isPointInside(mouseX, mouseY)) 
			{
				let {color, x, height} = item;

				this.buildColumn(this.ctx, {color, x, height, hoverColor: 'white'});
			} 
			else 
			{
				let {color, x, height} = item;
				this.buildColumn(this.ctx, {color, x, height, hoverColor: 'black'});
			}
		})
		
	}


	render() {
		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h1 className="App-title">Bar chart</h1>
				</header>

				<canvas className="barChart" ref={e => this.canvas = e} width={CANVAS.WIDTH} height={CANVAS.HEIGHT} onMouseMove={this.onMouseMove}>

				</canvas>
			</div>
		);
	}
}

export default App;
